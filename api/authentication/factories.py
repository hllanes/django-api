from django.contrib.auth.models import User

import factory

class UserFactory(factory.django.DjangoModelFactory)
    username = factory.Sequence(lambda n: 'user_%s' % n)
    password = 'password'
