from django.contrib.auth.models import User

from rest_framework import status, generics, mixins
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.serializers import AuthTokenSerializer

from .serializers import UserSerializer

class CustomObtainAuthToken(ObtainAuthToken):
    """
    """


    def post(self, request):
        serializer = AuthTokenSerializer(data = request.data)
	serializer.is_valid(raise_execption=True)
	user = serializer.validate_data['user']
	token, created = Token.objects.get_or_created(user=user)
	user_data = UserSerializer(user).data
	user_data['token'] = token.key
	return Response(user_data)


class UserListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()

class UserRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
