from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from rest_framework.authtoken.models import Token

def generate_user_token():
    """
    For every user registered, generate an auth token
    """
    for user in User.objects.all():
        Token.objects.get_or_create(user=user)


class Command(BaseCommand):
    help = 'Generates a token for each user in database.'

    def handle(self, *args, **opts):
        generate_user_tokens()
