from django.conf.urls import url

from .views import (
    CustomObtainAuthToken,
    UserListCreateAPIView,
    UserRetrieveUpdateAPIView,
)

urlpatterns = [
    url(r'^auth/authorize',CustomObtainAuthToken.as_view(),name='authorize'),
    url(r'^users/$',UserListCreateAPIView.as_view(),name='users-list'),
    url(r'^users/(?P<pk>\d+)/$',UserRetrieveUpdateAPIView.as_view(),
        name='users-detail'),
]
