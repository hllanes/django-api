from django.contrib.auth.models import User

from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    """
    This serializer will contain private data for a
    :class: 'django.contrib.auth.models.User' and so it will only be available
    via authentication.
    """

    class Meta:
        model = User
	fields = ('username','id')
