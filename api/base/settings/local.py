from os.path import join, dirname

from configurations import values

from .base import Base

BASE_DIR = dirname(dirname(dirname(__file__)))

class Local(Base):
    DEBUG = values.BooleanValue(True)

    INSTALLED_APPS = Base.INSTALLED_APPS 

    INSTALLED_APPS +=(
        'debug_toolbar',
        'django_extensions'
    )
    REST_FRAMEWORK = Base.REST_FRAMEWORK
    REST_FRAMEWORK['TEST_REQUEST_DEFAULT_FORMAT'] = 'json'


